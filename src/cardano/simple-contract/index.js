import Cardano from "../serialization-lib";
import { contractAddress } from "./validator";
import {
  assetsToValue,
  createTxOutput,
  finalizeTx,
  initializeTx,
} from "../transaction";
import { fromHex } from "../../utils/converter";

export const sendLovelace = async (
  lovelaceAmount,
  walletAddress,
  walletUtxos,
) => {
  try {
    const { txBuilder, datums, outputs } = initializeTx();

    const utxos = walletUtxos.map((utxo) =>
      Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
    );

    outputs.add(
      createTxOutput(
        contractAddress(),
        assetsToValue([
          {
            unit: "lovelace",
            quantity: lovelaceAmount
          },
        ]),
      )
    );

    const txHash = await finalizeTx({
      txBuilder,
      datums,
      utxos,
      outputs,
      changeAddress: Cardano.Instance.Address.from_bech32(walletAddress),
    });

    return { txHash };
  } catch (error) {
    console.error(error);
  }
};
