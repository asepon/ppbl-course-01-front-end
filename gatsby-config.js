module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "gimbalabs ppbl course 01",
  },
  plugins: [
    "gatsby-transformer-sharp",
    "@chakra-ui/gatsby-plugin",
    {
      resolve: "gatsby-plugin-apollo",
      options: {
        uri: "https://d.graphql-api.testnet.dandelion.link/",
      },
    },
  ],
};
